import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
  const data = {
    title: "Budget Expenses Tracker",
    content: "Record Income/Expenses. Monitor Income/Expenses. Save Money."
  }
  return (
    <>
      <Head>
        <title>Budget Expenses Tracker</title>
      </Head>
      <Banner data={data} />
    </>
  )
}