import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container  } from 'react-bootstrap';
import Router from 'next/router'
import UserContext from '../UserContext';
export default function create() {
    let token = "";
    
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
   
    const { user, setUser } = useContext(UserContext)
    useEffect(() => {
        token = localStorage.getItem('token')
    },[categoryName, categoryType])
    
   
    function createCategory(e) {
        e.preventDefault();
        console.log(`${categoryName}  type: ${categoryType}.`);
        setCategoryName('');
        setCategoryType('');
        fetch(`http://localhost:4000/api/users/add-category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${token}`
            },
            body: JSON.stringify({
                name: categoryName,
                typeName: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(categoryType)
            Router.push('/categories')
        })
    }
    
    return (
        <Container>
            <Form onSubmit={(e) => createCategory(e)}>
                <Form.Group controlId="categoryName">
                    <Form.Label>Category Name:</Form.Label>
                    <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="categoryType">
                    <Form.Label>Category Type:</Form.Label>
                    <Form.Control as="select" type="text" placeholder="Seclect type of category" onClick={e => setCategoryType(e.target.value)} required>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </Form.Group>
                <Button variant="primary" type="submit">Add</Button>
            </Form>
        </Container>
    )
}