import { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'
import moment from 'moment'

export default function LineChartBalance({rawData}){
    const [monthlyBalance, setMonthlyBalance] = useState([])
    const [range, setRange] = useState([])

    useEffect(() => {
        if(rawData.length > 0){

            const balance = rawData.map(data => {
                return data.balanceAfterTransaction
            })

            balance.reverse()

            setMonthlyBalance(balance)
        }
    }, [rawData])

    useEffect(() => {
        setRange([0, 15000, 30000, 45000, 60000, 75000, 90000, 100000])
        
    }, [monthlyBalance])

    const data = {
        labels: monthlyBalance,
        datasets: [
            {
                label: 'Monthly Balance for the Year 2020',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: monthlyBalance
            }
        ]
    }

    return(
        <Line data={data}/>
    )
}